import { Injectable } from '@angular/core';
declare var Keycloak: any;

@Injectable()
export class KeycloakService {
  static auth: any = {};
  static init(logoutUrl): Promise<any> {


    let loadedKC:any = {};
           
          function getUrlParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
          };

          let getParamObj: any = {};
          if (getUrlParameter('clientId') == "") {

            let getLocalInfo = localStorage.getItem('getLocalKCInfo');
            if (getLocalInfo) {

              loadedKC = JSON.parse(getLocalInfo);
       
            }
            else {
              window.location.href = logoutUrl;
            }

          }
          else {
            getParamObj.clientId = getUrlParameter('clientId');
            getParamObj.customerId = getUrlParameter('customerId');
            getParamObj.portalUserId = getUrlParameter('portalUserId');
            getParamObj.portalAccountName = getUrlParameter('portalAccountName');
            getParamObj.realmName = getUrlParameter('realmName');
            getParamObj.serverUrl = getUrlParameter('serverUrl');
            getParamObj.access_token = getUrlParameter('access_token');
            getParamObj.id_token = getUrlParameter('id_token');
            getParamObj.expires_in = getUrlParameter('expires_in');
            getParamObj.refresh_token = getUrlParameter('refresh_token');
            getParamObj.token_type = getUrlParameter('token_type');
            getParamObj.session_state = getUrlParameter('session_state');
            getParamObj.refresh_expires_in = getUrlParameter('refresh_expires_in');
            getParamObj.subAccountId = getUrlParameter('subAccountId');

            if (localStorage.getItem('getLocalKCInfo')) {
              localStorage.setItem('getLocalKCInfo', JSON.stringify(getParamObj));
            }
            else {
              localStorage.setItem('getLocalKCInfo', JSON.stringify(getParamObj));
            }
            
            loadedKC = getParamObj
          }



  let kyclk = {
    "realm"             : loadedKC.realmName,
    "url"               : loadedKC.serverUrl,
    "ssl-required"      : "external",
    "clientId"          : loadedKC.clientId,
    "public-client"     : true,
    "confidential-port" : 0,
    "logoutUrl"         : logoutUrl
  }

  let keycloakAuth: any = new Keycloak(kyclk);

    KeycloakService.auth.roles = {};
    KeycloakService.auth.userInfo = {}; 
    KeycloakService.auth.portalInfo = {}; 
    KeycloakService.auth.customKeycloakLogoutUrl = logoutUrl;
      return new Promise((resolve, reject) => {
        keycloakAuth.init({
             token: loadedKC.access_token,
             refreshToken: loadedKC.refresh_token,
             idToken: loadedKC.id_token,
             checkLoginIframe: false
           })
          .success(() => {
            KeycloakService.auth.loggedIn = true;
            KeycloakService.auth.authz = keycloakAuth;             
            KeycloakService.auth.portalInfo.customerId = loadedKC.customerId;
            KeycloakService.auth.portalInfo.portalAccountName = loadedKC.portalAccountName;
            KeycloakService.auth.portalInfo.portalUserId = loadedKC.portalUserId;
            KeycloakService.auth.portalInfo.subAccountId = loadedKC.subAccountId;           
            keycloakAuth.loadUserInfo().success(function(userInfo) {             
              KeycloakService.auth.userInfo = userInfo;
             // debugger;
              sessionStorage.user_name=userInfo.preferred_username;
              sessionStorage.displayName=userInfo.name;
              resolve();              
          }).error(function() {
              console.log('Failed to load user info');
          })
           
          })
          .error(() => {
            console.log('invalid');
            window.location.href =  KeycloakService.auth.customKeycloakLogoutUrl
            reject();
          });
      });
    }

  getRoles(name){
    return KeycloakService.auth.authz.hasResourceRole(name);
  }

  isRoleAssigned(){
    const authInfo =  KeycloakService.auth.authz;
    const clientId = authInfo.resourceAccess[authInfo.clientId];
    if(authInfo && authInfo.resourceAccess && authInfo.resourceAccess[authInfo.clientId]){
      const roles = authInfo.resourceAccess[authInfo.clientId]['roles'];     
      return roles.length ? true  : false;
    }
    else
    return false;
  }

  getLoginInfo(){
    return KeycloakService.auth.userInfo;
  }
  getPoratlInfo(){
    return KeycloakService.auth.portalInfo;
  }
  getPortalInfo(){
    return KeycloakService.auth.portalInfo;
  }

  logout() {
    console.log('*** LOGOUT');
    localStorage.removeItem('getLocalKCInfo');
    KeycloakService.auth.loggedIn = false;
    var options={redirectUri :KeycloakService.auth.logoutUrl}
    KeycloakService.auth.authz.logout(options);
    window.location.href = KeycloakService.auth.customKeycloakLogoutUrl;

  }
  
  getToken(): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      if (KeycloakService.auth.authz.token) {
        KeycloakService.auth.authz.updateToken(5)
          .success(() => {
            resolve({token:KeycloakService.auth.authz.token, customerId:KeycloakService.auth.portalInfo.customerId});
          })
          .error(() => {
            reject('Failed to refresh token');
          });
      }
    });
  }
}
