import { Component, OnInit, ViewChild} from '@angular/core';
import { SharedService } from '../../shared/shared.service';
import { ExceptionHandlingService } from '../../shared/exception-handling.service';
import { AppConstants } from '../../config.service';
import { IMyOptions } from 'ng-uikit-pro-standard';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from 'ng-uikit-pro-standard';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-deliverables',
  templateUrl: './deliverables.component.html',
  styleUrls: ['./deliverables.component.scss'],
  providers: [SharedService]
})
export class DeliverablesComponent implements OnInit {

  search:any={
    dateSearchStart:new Date(),
    dateSearchEnd:new Date()
  };
  searchResults:any=[];
  searchResultsLoading:boolean=false;
  paginationProperty: any;
  pageInfo = {
    id: 'deliverables-components-pagination',
    itemsPerPage: 10,
    currentPage: 1,
    totalItems: 0,
    disablePagination: false
  };
  searchResultsMessage:string='';
  deliverableTypes:any=[];
  order:any={};
  public datePickerOptions: IMyOptions = {
    // Your options
    dateFormat:'mmm-dd-yyyy'
  };
  constructor(
    private service: SharedService,
    private appConstants: AppConstants,
  ) { }

  ngOnInit() {
    this.setUI();
  }
  setUI(){
    let h = window.innerHeight;
    document.getElementById('searchResults').style.height = (h-260) + 'px';
  }

  searchSelections(){
    //TODO:
  }

  deliverableTypesLoading:boolean=false;
  getDeliverableTypes(){
    //TODO:
  }

  downloadPDF(){
    //TODO:
  }

  @ViewChild('viewPDFModal',{static:true}) viewPDFModal:any;
  openViewPDFModal(){
    this.viewPDFModal.show();
  }
  pdfData:any={};
  viewerOpened(){
    let h = window.innerHeight;
    const uiInterval = setInterval(() => {     
      document.getElementById('pdfview-container').style.height = (h-250) + "px";          
      clearInterval(uiInterval)
    }, 100)
  }
  group:any={};
  contactGroups:any[];
  groupsLoading:boolean=false;
  @ViewChild('sharePDFModal',{static:true}) sharePDFModal:any;
  openSharePDFModal(){
    this.sharePDFModal.show();
  }
  getGroups(){
    //TODO:
  }


}
