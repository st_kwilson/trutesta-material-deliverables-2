
import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants } from './config.service';
import { SharedService } from './shared/shared.service';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile, KeycloakRoles } from 'keycloak-js';
@Injectable()
export class AuthService  implements OnInit  {
    userDetails: KeycloakProfile;
    roles: Array<any> = [];
    constructor(
        private keycloakService: KeycloakService,
        private router: Router,
        private sharedService: SharedService,
        private appConstants:AppConstants
    ) { }
    redirectUrl: any;
    async ngOnInit() {
        if (await this.keycloakService.isLoggedIn()) {
          this.userDetails = await this.keycloakService.loadUserProfile();
          this.roles = await this.keycloakService.getUserRoles();
          //console.log(roles.indexOf("activityUndo") <0 ? false : true)
          const tokens = await this.keycloakService.getToken();
        }
      }

    public canSchedule() {
       return this.roles.indexOf("canSchedule") <0 ? false : true;      
    }

    checkRequestAccessRole(){
        return this.roles.indexOf('requests') <0 ? false : true;
    }
    checkSubmittedRequestAccessRole(){
        return this.roles.indexOf('approver') <0 ? false : true;
    }
    checkCanSchedules() {
        return this.roles.indexOf('schedules') <0 ? false : true;
    }
    checkSettingsRole(){
        return this.roles.indexOf('settings') <0 ? false : true;
    }
    checkProductlinesRole(){
        return this.roles.indexOf('productlines') <0 ? false : true;
    }
    checkAddressbookRole(){
        return this.roles.indexOf('addressbook') <0 ? false : true;
    }
    checkContactsRole(){
        return this.roles.indexOf('contacts') <0 ? false : true;
    }
    checkProductsRole(){
        return this.roles.indexOf('products') <0 ? false : true;
    }
    checkRequestCertRole(){
        return this.roles.indexOf('requestcert') <0 ? false : true;
    }
    checkProductDocsRole(){
        return this.roles.indexOf('productdocs') <0 ? false : true;
    }
    checkProductMfgRole(){
        return this.roles.indexOf('productmfg') <0 ? false : true;
    }
    canViewNewFeatures() {
        return this.roles.indexOf('canViewNewFeatures') <0 ? false : true;
    }
    checkAdminRole(){
        return this.roles.indexOf('adminRole') <0 ? false : true;
    }

    async checkRoleExist(role){
        if (await this.keycloakService.isLoggedIn()) {
            return this.keycloakService.getUserRoles().indexOf(role) <0 ? false : true;
        }
        }

    isDiscardQuoteRequestRoute = false;
    canDiscardQuoteRequestRoute(val){
        this.isDiscardQuoteRequestRoute = val;
        console.log(this.isDiscardQuoteRequestRoute)
    }

    public resourceCheck() {
        return new Promise((resolve, reject) => {
           // let keycloakUserInfo = this.keycloakService.getLoginInfo();     
            let resourceName = this.userDetails;
            if(resourceName)
             this.sharedService
               .get(this.appConstants.endPoints.resourcesEndPoint+'resources/'+ resourceName,null)
               .subscribe(data => {
                resolve(true)
               },
               error => {
                 if(error.response.status == 400)
                 resolve(false)
               });
        });

    }
}