import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, NavigationExtras, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class OnlyAuthUsersGuard implements CanActivate {
  constructor(private userService: AuthService, private router: Router,
   ) { };

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    let url: string = state.url;
    const isAuth = this.userService.canSchedule();
    return  this.userService.resourceCheck().then(res =>{
    if (isAuth)
      return true;
    else if (isAuth != undefined && !isAuth) {
      this.router.navigate(['/invalid']);
      return false;
    }
    else
      return false;
    })


  }

}

@Injectable()
export class QuoteRequestAuthGuard implements CanActivate {
  constructor(private userService: AuthService, private router: Router) { };

  canActivate() {
    return this.userService.checkRequestAccessRole();
  }

}

@Injectable()
export class QuoteSubmittedRequestAuthGuard implements CanActivate {
  constructor(private userService: AuthService, private router: Router) { };

  canActivate() {
    return this.userService.checkSubmittedRequestAccessRole();
  }

}


@Injectable()
export class QuoteSubmittedRequestChildAuthGuard implements CanActivateChild {
  constructor(private userService: AuthService, private router: Router) { };

  canActivateChild() {
    return this.userService.checkSubmittedRequestAccessRole();
  }

}
@Injectable()
export class QuoteRequestChildAuthGuard implements CanActivateChild {
  constructor(private userService: AuthService, private router: Router) { };

  canActivateChild() {
    return this.userService.checkRequestAccessRole();
  }

}

@Injectable()
export class SchedulesAuthGuard implements CanActivate {
  constructor(private userService: AuthService, private router: Router) { };

  canActivate() {
    return this.userService.checkCanSchedules();
  }

}
@Injectable()
export class ProductLineAuthGuard implements CanActivate {
  constructor(private userService: AuthService, private router: Router) { };

  canActivate() {
    return  this.userService.checkSettingsRole() && this.userService.checkProductlinesRole();
  }

}
@Injectable()
export class AddressBookAuthGuard implements CanActivate {
  constructor(private userService: AuthService, private router: Router) { };

  canActivate() {
    return this.userService.checkSettingsRole() && this.userService.checkAddressbookRole();
  }

}
@Injectable()
export class ContactAuthGuard implements CanActivate {
  constructor(private userService: AuthService, private router: Router) { };

  canActivate() {
    return this.userService.checkSettingsRole() && this.userService.checkContactsRole();
  }

}
@Injectable()
export class ProductAuthGuard implements CanActivate {
  constructor(private userService: AuthService, private router: Router) { };

  canActivate() {
    return this.userService.checkSettingsRole() && this.userService.checkProductsRole();
  }

}