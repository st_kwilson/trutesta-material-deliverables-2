import { ViewChild, Component, Output, EventEmitter } from '@angular/core';
import {MDBModalService, MDBModalRef } from 'ng-uikit-pro-standard';
import { TranslateService } from '@ngx-translate/core';
@Component({
    selector: 'common-delete-modal',
    templateUrl: 'common-delete.html',
    styles: [`
    .modal-header {        
        padding: 0.5rem 0.5rem;
        #commonDeleteModal {
            z-index:1051 !important;
        }
    `]
})

export class CommonDeleteModalComponent {

    @ViewChild('commonDeleteModal',{static:true}) 
    commonDeleteModal: any;
    operation:any;
    deleteItem: any = {};
    commonDeleteHeading:any;
    commonDeleteMessage:any;
    isLoading:boolean = false;
    @Output('commonDelete') deleteConfirm = new EventEmitter<string>();

    constructor(
      private modalService: MDBModalService
    ) { }

    open(obj, deleteHeading, deleteMessage){
        this.deleteItem = obj;
        this.commonDeleteHeading = deleteHeading;
        this.commonDeleteMessage = deleteMessage;
        this.commonDeleteModal.show()
    }

    close(){
        this.commonDeleteModal.hide();
    }

    confirmDelete(status){
        if(!status){
            this.close();
        }
        this.deleteConfirm.emit(status);
    }


}