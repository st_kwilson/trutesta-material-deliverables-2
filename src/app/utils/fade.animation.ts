import { trigger, state, style, animate, transition } from '@angular/animations';

export const fadein = [
    trigger('fadeInOut', [
        state('true', style({ opacity: 1 })),
        state('false', style({ opacity: 0 })),
        transition('true => false', animate('0s')),
        transition('false => true', [animate('400ms')] ),
    ])
];
