import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-spinner',
    template: `
    <div class="view loader-container" *ngIf="isLoading">
  <div class="mask rgba-white-strong flex-center">
    <div class="spinner-border text-dark" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>
</div>
    `
})
export class SpinneerComponent {
    @Input() isLoading: boolean = false;
}
