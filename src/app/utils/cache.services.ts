import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class CacheService {
    public responseCache = new Map();

    constructor(private http: HttpClient) { }

    public get(URL): Observable<any> {
        const cachedData = this.responseCache.get(URL);
        if (cachedData) {
            return of(cachedData);
        }
        const response = this.http.get<any>(URL);
        response.subscribe(response => this.responseCache.set(URL, response));
        return response;
    }
}