import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth.service';
import { DeactivateGuard } from './auth-guard-deactivate.service';
import { OnlyAuthUsersGuard } from './auth-guard.service';

import { DeliverablesComponent } from './views/deliverables/deliverables.component'

const routes: Routes = [
 
  {
    path: 'deliverables',
    component: DeliverablesComponent
  },
  {
    path: '',
    redirectTo:'/deliverables',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  providers: [AuthService, OnlyAuthUsersGuard]
})
export class RoutingModule { }
