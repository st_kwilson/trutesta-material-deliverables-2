
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { KeycloakService } from 'keycloak-angular';
@Injectable()
export class AppConstants {
  endPoints: any = {};
  constructor(private http: HttpClient, private keycloakService: KeycloakService) { }
  endApIPoints() {
    this.http.get('./assets/appConfig/apiservices.json').subscribe((res: any) => {
      this.endPoints = res['constants']
      return this.endPoints;
    });
  }

  async load() {
    const keycloak: any = await this.getKeycloakConfig();
    return new Promise(async (resolve, reject) => {
      try {

        await this.keycloakService.init({
          config: keycloak,
          initOptions: {
            onLoad: 'login-required',
            checkLoginIframe : false
          },
          bearerExcludedUrls: []
        });
        resolve();
      } catch (error) {
        reject(error);
      }
    });

  }

  getKeycloakConfig() {
    return new Promise((resolve, reject) => {
      this.http.get('./assets/appConfig/keycloak.json').subscribe((res: any) => {
        resolve(res);
      });
    });
  }

}