import { Component,OnInit} from '@angular/core';
import { ToastService }     from 'ng-uikit-pro-standard';
import { KeycloakService }  from 'keycloak-angular';
import { AppConstants } from '../config.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public user: any;
  public userEmailAddress: any = {};
  public loggedInUserInitials:string;
  constructor(
    private keycloakService: KeycloakService,
    public toastr: ToastService,
    private appConstants: AppConstants
  ) {

  }


  async ngOnInit() {
    if (await this.keycloakService.isLoggedIn()) {
      const user = await this.keycloakService.loadUserProfile();
      this.userEmailAddress = user.email
      this.user = user.firstName + ' ' + user.lastName
      this.loggedInUserInitials = this.user.match(/\b(\w)/g).join('');
    }
  }

  async logout() {
    await this.keycloakService.logout();
  }

  
  public msgCount:number;
  notificationInfo:any={};
  isDrawerOpen = false;

  async toggleShowDrawer() {
    this.isDrawerOpen = !this.isDrawerOpen;
    let notificationsData;
    if (this.isDrawerOpen) {
      const token = await this.keycloakService.getToken();
      notificationsData = {
        headers: {
          token: token,
          customerId: "",
        },
        unread: {
          url: this.appConstants.endPoints.notificationEndPoint + 'all',
          params: { destinationAddress: this.userEmailAddress }
        },
        markAsRead: {
          url: this.appConstants.endPoints.notificationEndPoint + 'mark-as-read',
          params: { destinationAddress: this.userEmailAddress }
        },
        markAllRead: {
          url: this.appConstants.endPoints.notificationEndPoint + 'mark-all-read',
          params: { destinationAddress: this.userEmailAddress }
        }
      }
      notificationsData['isOpen'] = true;
      this.notificationInfo = { ...{}, ...notificationsData };
    }
    else {
      this.notificationInfo = { isOpen: false };
    }
  }
  // toggleShowDrawer(){
  //   //debugger;
  //   this.isDrawerOpen = !this.isDrawerOpen;
  //   if(this.isDrawerOpen){
  //       const obj = {};       
  //       obj['isOpen'] = this.isDrawerOpen;  
  //       this.notificationInfo = Object.assign({}, obj);
  //   }
  //   else{
  //     const obj = {};      
  //     obj['isOpen'] = this.isDrawerOpen;
  //     this.notificationInfo = Object.assign({}, obj);
  //   }    
  // }
  closeNotification(e){
    // if(e){
    //   this.setNotificationBadge(e)
    // }else{
      this.isDrawerOpen=false;
    // }
    
  }
  setNotificationBadge(e){
    this.msgCount = e
  }

}
