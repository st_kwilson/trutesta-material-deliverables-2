import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../shared/shared.service';
import { AppConstants } from '../../../config.service';
import { ToastService }   from 'ng-uikit-pro-standard';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'underscore';
import { ExceptionHandlingService } from '../../../shared/exception-handling.service';
import { SprigService } from './sprig.service';
import { Subscription } from 'rxjs';


@Component({
    selector: 'refresh-document-service',
    templateUrl: 'sprig.component.html',
    styles: [`a {
        border-color: transparent;
        border-style: solid;
        border-width: 1px 0;
        padding: 1px 10px;
        color: #363636;
        display:block;

    }
    a:hover, a:focus {
        text-decoration: none;
        color: #4d5258;
        background-color: #def3ff;
        border-color: #bee1f4;
    }`]
})

export class SprigComponent implements OnInit {

    subscription: Subscription;
    constructor(
        private sharedService: SharedService,
        private appConstants: AppConstants,
        public toastr: ToastService,
        private errorService: ExceptionHandlingService,
        private translate: TranslateService,
        private SprigService: SprigService
    ) {
        this.subscription = this.SprigService.uploadFileCount.subscribe(message => {
            this.setData(message);
        });
    }
    
    sprigDevicesList: any;
    ngOnInit() {
        this.loadDevices();
    }
    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }

    uploadCount: number;
    setData(message) {
        this.uploadCount = message;
        this.SprigService.setData('count', this.uploadCount);
        if (message == 0) {
            this.deviceDetails = Object.assign({}, this.lastDeviceDetails);
            this.SprigService.setData('icon', this.deviceDetails);
        }
        else {
            this.deviceDetails = Object.assign({}, this.lastDeviceDetails);
            this.SprigService.setData('icon', this.deviceDetails);
        }
    }

    ngOnChanges() {

    }

    toastOptions = {
        positionClass:'md-toast-bottom-right',tapToDismiss: true 
      }
    healthCheckStatus() {
        return new Promise((resolve, reject) => {
            const healthCheckStatus = [];
            const sprigHealthChekObj = Object.assign([], this.sprigDevicesList)
            for (var k = 0; k < sprigHealthChekObj.length; k++) {
                var loopDevice = sprigHealthChekObj[k];
                this.sharedService
                    .getCore(loopDevice.healthCheckUrl, {},true)
                    .subscribe(data => {
                        healthCheckStatus.push({ healthCheckStatus: data.status, healthCheckStatusUrl: data.url })
                        if (sprigHealthChekObj.length == healthCheckStatus.length)
                            resolve(healthCheckStatus);
                    }, data => {

                        healthCheckStatus.push({ healthCheckStatus: data.statusCode, healthCheckStatusUrl: data.url })
                        if (sprigHealthChekObj.length == healthCheckStatus.length)
                            reject(healthCheckStatus);
                    });

            }
        });
    }

    sprigDevice: any;
    updateProgress: any = false;
    responseData: any = [];
    requestData: any = [];
    lastDeviceDetails: any = {};
    deviceDetails: any = {};
    loadDevices() {
        localStorage.removeItem('isSprigEnabled');
        localStorage.removeItem('selectedDevice');
        this.sprigDevicesList = [];
        this.deviceDetails = { icon: "sync-alt fa-spin fa-fw", response: "Loading" };
        this.lastDeviceDetails = Object.assign({}, this.deviceDetails);
        this.SprigService.setData('icon', this.deviceDetails)
        this.updateProgress = true;
        this.responseData = [];
        this.uploadCount = 0;
        this.getSprig().then(devices => {
            this.sprigDevicesList = devices;
            this.sprigDevice = [];
            let requestTime;
            let count = 0;
            this.requestData = [];
            this.healthCheckStatus().then((result: any) => {
                const res = result;
                for (let i = 0; i < this.sprigDevicesList.length; i++) {
                    for (let j = 0; j < res.length; j++) {
                        if (this.sprigDevicesList[i].healthCheckUrl == res[j].healthCheckStatusUrl) {
                            this.sprigDevicesList[i]['healthCheckStatusCode'] = res[j].healthCheckStatus;
                        }
                    }
                }
                for (var i = 0; i < this.sprigDevicesList.length; i++) {
                    var loopDevice = this.sprigDevicesList[i];

                    loopDevice['responseTime'] = 0;
                    loopDevice['status'] = 0;
                    loopDevice['requestTime'] = new Date().getTime();
                    this.requestData.push(loopDevice);

                    if (loopDevice.healthCheckStatusCode == 200) {
                        this.sharedService
                            .getCore(loopDevice.pingUrl + '?sprigDeviceID=' + loopDevice.installationId, {},true)
                            .subscribe(data => {
                                const url = data.url.substr(0, data.url.indexOf('?'));
                                this.responseData.push({ status: data.status, cleanedUrl: url, responseTime: new Date().getTime() })
                                if (this.sprigDevicesList.length == this.responseData.length)
                                    this.getReturnData()
                            }, err => {
                                const url = err.url.substr(0, err.url.indexOf('?'));
                                this.responseData.push({ status: err.status, cleanedUrl: url, responseTime: new Date().getTime() })
                                if (this.sprigDevicesList.length == this.responseData.length)
                                    this.getReturnData()
                            });
                    }
                    else {
                        if (this.sprigDevicesList.length == i + 1) {
                            this.noSprigService();
                        }
                    }
                }

            }, err => {
                this.noSprigService()
                console.log(err)
            })
        }).catch(res => {
        });

    }

    getReturnData() {
        const req = this.requestData;
        const res = this.responseData;
        for (let i = 0; i < req.length; i++) {
            const reqLoop = req[i];
            for (let j = 0; j < res.length; j++) {
                const resLoop = res[j];
                if (reqLoop.pingUrl == resLoop.cleanedUrl) {
                    reqLoop.responseTime = resLoop.responseTime;
                    reqLoop.status = resLoop.status;
                    // const difference = (reqLoop.responseTime - resLoop.requestTime) / 1000;
                    //  reqLoop.totalTimeDiff = difference;
                }
            }
        }

        const finalSprigDevice = [];
        for (let i = 0; i < req.length; i++) {
            const device = req[i];
            if (device.status == 200) {
                const difference = (device.responseTime - device.requestTime) / 1000;
                device['totalTimeDiff'] = difference;
                finalSprigDevice.push(device);
            }
        }

        var sortedDevice = _.sortBy(finalSprigDevice, function (item) {
            return item['totalTimeDiff'];
        });

        if (sortedDevice.length > 0) {
            this.selectedDevice = sortedDevice[0];
            localStorage.setItem('isSprigEnabled', "true");
            localStorage.setItem('selectedDevice', JSON.stringify(this.selectedDevice));
            this.SprigEnabled = localStorage.getItem('isSprigEnabled'); // used by icon in masthead            
            this.toastr.success(this.selectedDevice.name + " " + this.translate.instant("administration_connectedSuccessfully_notification"),'',this.toastOptions);
            if (this.selectedDevice.isAppliance) {
                this.deviceDetails = { icon: "leaf", response: this.selectedDevice.name };
            }

            else {
                this.deviceDetails = { icon: "cloud", response: this.selectedDevice.name };
            }
            this.lastDeviceDetails = Object.assign({}, this.deviceDetails);
            this.SprigService.setData('icon', this.deviceDetails);
        }
        else {
            this.noSprigService();
        }
        this.updateProgress = false;
    }

    noSprigService() {
        // need better error if sortedDevice is null
        this.deviceDetails = { icon: "cloud", response: "Document Service NOT available" };
        this.lastDeviceDetails = Object.assign({}, this.deviceDetails);
        this.selectedDevice = { isAppliance: '' };
        localStorage.setItem('isSprigEnabled', "false");
        localStorage.setItem('selectedDevice', '');
        this.updateProgress = false;
        this.SprigService.setData('icon', this.deviceDetails)
    }

    finalValues: any = [];
    SprigEnabled: any = false;
    selectedDevice: any = { isAppliance: '', };
    error: any;
    getSprig() {
        return new Promise((resolve, reject) => {

            this.sharedService
                .getCore(this.appConstants.endPoints.sprigEndPoint + 'admin/appliances', { isActive: true })
                .subscribe(data => {
                    let devicesData = data.searchResults;
                    resolve(devicesData);
                }, res => {
                    let errors = res.error;
                    this.errorService.errorHandling(res).then(res => {
                        this.error = res;
                    });
                    reject(res)
                });
        });
    };
}