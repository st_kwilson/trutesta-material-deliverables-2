import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { SharedService } from '../../../shared/shared.service';
import { AppConstants } from '../../../config.service';

@Injectable({
    providedIn:'root'
})
export class SprigService {
    constructor(private sharedService: SharedService, private appConstants: AppConstants) { }
    uploadFileCount = new Subject<any>();
    sendData = new Subject<any>();

    sendCountData(data: any) {
        this.uploadFileCount.next(data);
    }
    setData(operation: string, data: any) {
        this.sendData.next({ operation, data });
    }
}