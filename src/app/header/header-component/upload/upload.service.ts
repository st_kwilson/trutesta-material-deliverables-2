import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn:'root'
})
export class UploadService {
    isUploading:boolean = false;
    constructor() { }
    sendData = new Subject<any>();
    receiveFiles = new Subject<any>();
    loadDocsList = new Subject<any>();
    output(operation: string, data: any) {
        this.sendData.next({ operation, data });
    }

}