import { Component, OnInit, HostListener } from '@angular/core';
import { ToastService }   from 'ng-uikit-pro-standard';
import { UploadService } from './upload.service';
import { SprigService } from '../sprig/sprig.service';
import { Observable, Subscription } from 'rxjs';
import { SharedService } from 'src/app/shared/shared.service';
import { FileQueueObject, FileUploaderService } from './file.upload.service';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { AppConstants } from 'src/app/config.service';
@Component({
    selector: 'upload-icon',
    templateUrl: 'upload.component.html',
    styles: [`
    a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }

    .danger {
        border: solid 1px #d9534f !important;
      }

    `]
})

export class UploadComponent implements OnInit {
    removeFailedFiles:Array<any> = [];

    @HostListener('window:unload', ['$event'])
    unloadHandler(event) {
        const url = this.appConstants.endPoints.sprigEndPoint + 'documents/' +
            this.appConstants.endPoints.applicationID + '/documents';
        let docIds = [];
        for (let index = 0; index < this.removeFailedFiles.length; index++) {
            const element = this.removeFailedFiles[index];
            docIds.push(element.id)
        }
        if (this.UploadService.isUploading) {
            let xhr = new XMLHttpRequest()
            xhr.open("DELETE", url, false);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Authorization", "bearer " + this.token);
            xhr.send(JSON.stringify(docIds));
        }
    }

    @HostListener('window:beforeunload', ['$event'])
    unloadBeforeHandler(event) {
        if(this.UploadService.isUploading)
        return false;
    }

    subscriptionSprig: Subscription;
    subscription: Subscription;
    popoverOpen: boolean = false;

    token:string;
    constructor(
        public toastr: ToastService,
        private UploadService: UploadService,
        private SprigService: SprigService,
        private service:SharedService,
        public uploader: FileUploaderService,
        private translate: TranslateService,
        private keycloakService: KeycloakService,
        private appConstants:AppConstants
    ) {
        this.subscription = this.UploadService.receiveFiles.subscribe(obj => {            
            this.upload(obj);
        })
        this.subscriptionSprig = this.SprigService.sendData.subscribe(res => {
            this.getSprigInfo(res);
        });

    }

    queue: Observable<FileQueueObject[]>;

    ngOnInit() {
        this.queue = this.uploader.queue;
        this.uploader.onCompleteItem = this.completeItem;
        this.uploader.onErrorCompleteItem = this.completeErrorItem;

    }

    fileCounter = 0;
    
    docs:Array<any> = [];

    completeErrorItem = (item: FileQueueObject, response: any) => {      
        const errorMsg = response.error.errorMessage;
        this.toastr.error(errorMsg);
        setTimeout(() => {
            this.popoverOpen = false;
            this.uploader.clearQueue();
            this.faIcon = this.oldFaIcon;
            this.fileCounter = 0;
        }, 1000);
    }

    completeItem = (item: FileQueueObject, response: any) => {
        const findIndex = this.removeFailedFiles.findIndex(res=> res.id === item.id)
        if(findIndex != -1){
            this.removeFailedFiles.splice(findIndex, 1) 
        }
        if(item && item.fieldName){
            this.fileCounter -= 1; 
            if(this.fileCounter == 0){
                this.toastr.success(this.docs.length + '  ' +
                this.translate.instant("administration_uploadDocSuccessfully_label"));
                setTimeout(() => {
                    this.popoverOpen = false;
                    this.uploader.clearQueue();
                    this.faIcon = this.oldFaIcon;
                    this.fileCounter = 0;
                    this.UploadService.loadDocsList.next(this.docs);
                    this.UploadService.isUploading = false;
                }, 1000);
               
            }
        }
      }

      uploadStatus(item){
        return item.isSuccess() ? 'success' : item.isPending() ?  'warning' : item.inProgress() ? 'info' : item.isError() ? 'danger' : ''  ;
      }

   async upload(obj) {    
    this.removeFailedFiles = Object.assign([], obj.upload)
  
    if (await this.keycloakService.isLoggedIn()) {
        this.token = await this.keycloakService.getToken();
        this.docs = [];
        this.docs = obj.upload;              
        this.uploader.clearQueue();
        this.faIcon = "cloud text-green";
        this.fileCounter = obj.upload.length;
        this.UploadService.isUploading = true;
        obj.upload.forEach(element => {
            this.uploader.addToQueue(element.body, element.url, element.fieldName, element.param, element.method, element.id);           
        });
        this.uploader.uploadAll()
          this.popoverOpen = true;
    }

   

}

    faIcon: string = "cloud text-green";
    lastIconDetail: any = {};
    uploadCount: number = 0;
    mdbTooltip: string = "";
    oldFaIcon:string;
    getSprigInfo(res) {
        if (res.operation == 'icon') {
            this.faIcon = res.data.icon;
            this.oldFaIcon = res.data.icon;
            this.mdbTooltip = res.data.response;
            this.lastIconDetail = { icon: res.data.icon, mdbTooltip: res.data.response };
        }

    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
        this.subscriptionSprig.unsubscribe();
    }
    checkStatus(outputFile){
        if(outputFile.responseStatus>=400 && outputFile.responseStatus<=502){
            outputFile.progress.data.percentage = 'error';
            return 'danger';
        }
        return 'success';
    }
}