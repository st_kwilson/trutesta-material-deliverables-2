
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { AppComponent } from './app.component';
import { RoutingModule } from './app-routing.module';
import { ToastModule } from 'ng-uikit-pro-standard';
import { KeycloakAngularModule } from 'keycloak-angular';
import { SharedModule } from './shared/shared.module';
import { ExceptionHandlingService } from './shared/exception-handling.service';
import { AppConstants } from "./config.service";
import { UUIDService } from './shared/uuid.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpTokenInterceptor } from './interceptors';
import { LazyElementsModule } from '@angular-extensions/elements';
import { DeliverablesComponent } from './views/deliverables/deliverables.component'


export function initConfig(config: AppConstants) {
  return () => config.endApIPoints();
}

export function keycloakInitConfig(keycloak: AppConstants) {
  return () => keycloak.load();
}


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DeliverablesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    KeycloakAngularModule,
    ToastModule.forRoot({ positionClass: 'md-toast-bottom-right' }),
    FormsModule,
    LazyElementsModule,
    SharedModule,
    HttpClientModule,
    RoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    AppConstants, ExceptionHandlingService, UUIDService,
    { provide: APP_INITIALIZER, useFactory: initConfig, deps: [AppConstants], multi: true },
    {
      provide: APP_INITIALIZER,
      useFactory: keycloakInitConfig,
      multi: true,
      deps: [AppConstants]
    }

  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
