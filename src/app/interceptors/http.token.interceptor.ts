import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConstants } from '../config.service';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
  constructor(private appConstants: AppConstants) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headersConfig = {
      'X-TenantId': this.appConstants.endPoints && this.appConstants.endPoints.xTenantId ? this.appConstants.endPoints.xTenantId : ''
    };   
        const request = req.clone({ setHeaders: headersConfig });
        return next.handle(request);



  }
}
