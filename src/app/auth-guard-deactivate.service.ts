import { CanDeactivate } from '@angular/router';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
@Injectable()
export class DeactivateGuard implements CanDeactivate<any> {
  constructor(private authService : AuthService){

  }

  canDeactivate(authService : AuthService) {
    let can = this.authService.isDiscardQuoteRequestRoute;   
    if (can) {
      if(confirm('Are you sure you want to leave this page without saving?')){
        this.authService.canDiscardQuoteRequestRoute(false);
        return true;
      }
      else
      return false;

    }
    return true;
  }
}
