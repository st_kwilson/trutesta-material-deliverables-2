
import { Injectable } from '@angular/core';
import { ToastService } from 'ng-uikit-pro-standard';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';


export interface BlobFile extends Blob {
    name: string;
}
@Injectable({
    providedIn: 'root'
})

export class SharedService {
    constructor(
        private http: HttpClient,
        public toastr: ToastService,
        private translate: TranslateService
    ) {

    }

    get(path: string, params, responseType?: boolean): Observable<any> {
        let httpParams = new HttpParams({ fromObject: params });
        const options: any = { params: httpParams };
        if (responseType)
            options.observe = 'response';
        return this.http.get(path, options)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError())
            ); // end of pipe
    }

    getFile(url, param): Observable<any> {
        const options: any = { responseType: 'blob' };
        return this.http.get(url, options)
    }

    post(path: string, params, body: any, responseType?: boolean): Observable<any> {
        let httpParams = new HttpParams({ fromObject: params });
        const options: any = { params: httpParams };
        if (responseType)
            options.observe = 'response';
        return this.http.post(path, body, options)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError())
            ); // end of pipe
    }

    postText(url, data): Promise<any> {
        const options: any = { observe: 'response', responseType: 'text' };
        return this.http.post(url, data, options).toPromise()
    }

    postMultiPart(path: string, params, body: any, responseType?: boolean): Observable<any> {
        const options: any = { params: params };
        let formData = new FormData();
        Object.keys(body).forEach(function (key) {
            formData.append(key, JSON.stringify(body[key]));
        });
        if (responseType)
            options.observe = 'response';
        return this.http.post(path, formData, options)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError())
            ); // end of pipe
    }

    postFile(event: any): Observable<boolean> {
        const endpoint = event.url;
        const formData: FormData = new FormData();
        formData.append(event.fieldName, event.file, event.file.name);
        const options: any = { withCredentials: false,observe : 'response' };
        if(event.method == "PUT"){
            return this.http.put(endpoint, formData, options)
            .pipe(
              map(response => {
                  return response;
              }),
              catchError(this.handleError())
            ); // end of pipe
        }
        else if(event.method == "POST"){
            return this.http.post(endpoint, formData)
            .pipe(
              map(response => {
                  return response;
              }),
              catchError(this.handleError())
            ); // end of pipe
        }

    }
    requestWithFormData(path:string,method,data:any,headers){
        return new Observable(observe =>{
            var request = new XMLHttpRequest();
            request.open(method,path)
            request.addEventListener('error',(e: Event) => {
                observe.error(e);
                observe.complete();
            });
            request.onreadystatechange = () => {
                if (request.readyState === XMLHttpRequest.DONE) {
                    if(request.status!=204){
                        observe.error(JSON.parse(request.response))
                    }else{
                        observe.next({ type: 'done'});
                        observe.complete();
                    }
            }
            }
            try{
                Object.keys(headers).forEach(key => request.setRequestHeader(key, headers[key]));
                request.send(data)
            }
            catch (e) {
                observe.error(e);
            }
        })
    }

    put(path: string, params, body: any, responseType?: boolean): Observable<any> {
        let httpParams = new HttpParams({ fromObject: params });
        const options: any = { params: httpParams };
        if (responseType)
            options.observe = 'response';
        return this.http.put(path, body, options)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError())
            ); // end of pipe
    }

    putText(url, data): Promise<any> {
        const options: any = { observe: 'response', responseType: 'text' };
        return this.http.put(url, data, options).toPromise()
    }

    putMultiPart(path: string, params, body: any, responseType?: boolean): Observable<any> {
        const options: any = { params: params };
        let formData = new FormData();
        Object.keys(body).forEach(function (key) {
            formData.append(key, JSON.stringify(body[key]));
        });
        if (responseType)
            options.observe = 'response';
        return this.http.put(path, formData, options)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError())
            ); // end of pipe
    }

    delete(path: string, params, responseType?: boolean): Observable<any> {
        let httpParams = new HttpParams({ fromObject: params });
        const options: any = { params: httpParams };
        if (responseType)
            options.observe = 'response';
        return this.http.delete(path, options)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError())
            ); // end of pipe
    }

/*     postReport(url, data): Observable<any> {
        return fromPromise(this.kc.getToken())
        .mergeMap(token => {
                let headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
                headers = headers.set('Authorization', 'Basic ' + btoa("admin:test12345"));
                const options: any = { headers: headers, responseType: 'blob' };
                data.data.headers = { 'Authorization': 'Bearer ' + token }
                return this.http.post(url, data, options)
                .pipe(
                    map(response => {
                        return response;
                    }),
                    catchError(this.handleError())
                ); // end of pipe
            })
    }
 */
    handleError() {
        return (error: HttpErrorResponse): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            const message = (error.error instanceof ErrorEvent) ?
                error.error.message :
                `server returned code ${error.status} with body "${error.error}"`;
            // TODO: better job of transforming error for user consumption
            console.log(error)
            if (error.status == 502  || error.status == 503) {
                this.toastr.error(this.translate.instant("administration_serviceUnavailableError_label"));
            }
            // Let the app keep running by returning a safe result.
            return throwError(error);
        };
    }

    getCore(url, param, responseType?): Observable<any> {

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        let httpParams =new HttpParams({ fromObject: param });
        const options: any ={ params: httpParams, headers: headers };
        if (responseType)
            options.observe = 'response';
        return this.http
            .get(url, options)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError())
            ); // end of pipe
    }

}
