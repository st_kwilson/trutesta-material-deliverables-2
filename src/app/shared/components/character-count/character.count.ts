import { Component, OnInit, Input, OnChanges } from '@angular/core';
@Component({
    selector: 'character-count',
    templateUrl: './character.count.html',
    styles: [``]
})
export class CharacterCountComponent implements OnInit, OnChanges {
    totalCount: number = 0;
    @Input() maxLength: number = 0;
    @Input() getValue: string;
    ngOnInit() {

    }
    ngOnChanges() {
        this.totalCount = 0;
        if (this.getValue && this.getValue.length) {
            this.totalCount = this.getValue.length
        }

    }
}