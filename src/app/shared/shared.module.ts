//Core lib
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { NgxPaginationModule } from 'ngx-pagination';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';

import { DragulaModule } from 'ng2-dragula';
import { TruncatePipe } from '../shared/truncate.pipe';
import { NavTimeDirective } from '../shared/directives/itemNav.directive';
import { NoResultComponent } from '../shared/components/noresult';
import {CharacterCountComponent} from './components/character-count/character.count'

import { SprigComponent } from '../header/header-component/sprig/sprig.component';
import { UploadComponent } from '../header/header-component/upload/upload.component';
import { SpinneerComponent } from '../utils/loader';
import { CommonDeleteModalComponent } from '../common/modals';

import { LocalToUTcPipe } from './pipe/local-to-utc.pipe';
import { UtcToLocalPipe } from './pipe/utc-to-local.pipe';
import { SearchBoxFilter } from './pipe/arrray-filter';
import { FilterPipeModule } from 'ngx-filter-pipe';

import { Debounce } from './directives/debounce.directive';
import { DragDropDirective } from './directives/drag-drop.directive';



export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgxPaginationModule,
        NgSelectModule,
        FilterPipeModule,
        DragulaModule.forRoot(),
        MDBBootstrapModulesPro.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        })
    ],
    declarations: [
        Debounce,
        NoResultComponent,
        SpinneerComponent,
        NavTimeDirective,
        TruncatePipe,
        DragDropDirective,
        //header components
        SprigComponent,
        UploadComponent,
        //common modals
        CharacterCountComponent,
        LocalToUTcPipe,
        UtcToLocalPipe,
        SearchBoxFilter,
        CommonDeleteModalComponent
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        TranslateModule,
        MDBBootstrapModulesPro,
        NgSelectModule,
        NgxPaginationModule,
        NoResultComponent,
        SpinneerComponent,
        NavTimeDirective,
        TruncatePipe,
        DragulaModule,
        Debounce,
        DragDropDirective,
        //header components
        SprigComponent,
        UploadComponent,
        CharacterCountComponent,
        LocalToUTcPipe,
        UtcToLocalPipe,
        SearchBoxFilter,
        FilterPipeModule,
        CommonDeleteModalComponent
    ]
})
export class SharedModule {
    constructor(private translate: TranslateService) {
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('en');

        // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use('en');
    }
}

