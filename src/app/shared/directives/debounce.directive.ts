import { EventEmitter, ElementRef, OnInit, Directive, Input, Output } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map, distinctUntilChanged, debounceTime, } from 'rxjs/operators';

import { NgModel } from '@angular/forms';

@Directive({ selector: '[debounce]' })
export class Debounce implements OnInit {
   @Input() delay: number = 700;
   @Output() func: EventEmitter<any> = new EventEmitter();

   constructor(private elementRef: ElementRef, private model: NgModel) {
   }

   ngOnInit(): void {
       const eventStream = fromEvent(this.elementRef.nativeElement, 'keyup')

           .pipe(
               map((e: any) => e.target.value), // extract the value of the input

               debounceTime(this.delay), //only search after 250 ms
               distinctUntilChanged()

           )
       eventStream.subscribe(input => this.func.emit(input));
   }

}