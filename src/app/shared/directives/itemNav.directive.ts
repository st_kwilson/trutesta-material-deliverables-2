import { Component, OnChanges, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: "nav-item",
    templateUrl: 'itemNav-template.html',
    styles: [`
        nav-item {float:right}
    `]
})

export class NavTimeDirective implements OnChanges, OnInit {
    @Input() list: Array<any> = [];
    @Input() selectedItem: any = {};
    @Output() setItem = new EventEmitter<any>();
    listCollapsed: boolean;
    disableLeft: boolean;
    disableRight: boolean;
    index: number;
    constructor(
       
    ) { }
    ngOnInit() {
        this.disableNavButton();
    }
    ngOnChanges() {
        this.disableNavButton();
        if (this.selectedItem && this.selectedItem.id) {
            this.checkList();
        }
        else {
            this.disableNavButton();
        }
    }

    checkList() {
        if (this.list.length > 0) {
            this.index = this.list.findIndex(item => item.id === this.selectedItem.id);
            if (this.index == this.list.length - 1) {
                this.disableRight = true;
                this.disableLeft = false;
            }
            else if (this.index == 0) {
                this.disableRight = false;
                this.disableLeft = true;
            }
            else {
                this.disableRight = false;
                this.disableLeft = false;
            }
        } else {
            this.disableNavButton();
        }
    }

    disableNavButton() {
        this.disableLeft = false;
        this.disableRight = false;
    };

    previous() {
        if (this.list.length > 1)
            this.setItem.emit(this.list[this.index - 1]);
    };

    next() {
        if (this.list.length > 1)
            this.setItem.emit(this.list[this.index + 1]);
    };
    expand() {
        this.listCollapsed = !this.listCollapsed
        const element: any = document.getElementsByClassName('first-child')[0];
        if (element) {
            if (this.listCollapsed) {
                this.fadeOut(element, {
                    duration: 700,
                    complete: function () {
                        element.style.display = "none";
                    }
                })
            }
            else {
                this.fadeIn(element, {
                    duration: 700,
                    complete: function () {
                        element.style.display = "";
                    }
                })
            }
        }
    };

    /* Animation start */


    animate(options) {
        var start: any = new Date;
        var id = setInterval(function () {
            const now: any = new Date
            var timePassed: any = now - start;
            var progress = timePassed / options.duration;
            if (progress > 1) {
                progress = 1;
            }
            options.progress = progress;
            var delta = options.delta(progress);
            options.step(delta);
            if (progress == 1) {
                clearInterval(id);
                options.complete();
            }
        }, options.delay || 10);
    }

    easing() {
        return {
            linear: function (progress) {
                return progress;
            },
            quadratic: function (progress) {
                return Math.pow(progress, 2);
            },
            swing: function (progress) {
                return 0.5 - Math.cos(progress * Math.PI) / 2;
            },
            circ: function (progress) {
                return 1 - Math.sin(Math.acos(progress));
            },
            back: function (progress, x) {
                return Math.pow(progress, 2) * ((x + 1) * progress - x);
            },
            bounce: function (progress) {
                for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                    if (progress >= (7 - 4 * a) / 11) {
                        return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
                    }
                }
            },
            elastic: function (progress, x) {
                return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
            }
        }
    }

    fadeOut(element, options) {
        const self = this;
        var to = 1;
        this.animate({
            duration: options.duration,
            delta: function (progress) {
                progress = this.progress;
                return self.easing().swing(progress);
            },
            complete: options.complete,
            step: function (delta) {
                element.style.opacity = to - delta;
            }
        });
    }

    fadeIn(element, options) {
        const self = this;
        var to = 0;
        this.animate({
            duration: options.duration,
            delta: function (progress) {
                progress = this.progress;
                return self.easing().swing(progress);
            },
            complete: options.complete,
            step: function (delta) {
                element.style.opacity = to + delta;
            }
        });
    }

    /* Animation end */

}