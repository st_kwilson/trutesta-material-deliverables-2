import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'
@Pipe({
  name: 'localToUtc'
})
export class LocalToUTcPipe {
  transform(value: any) {
    return moment(value).utc().format('YYYY-MM-DDTHH:mm:ss') + 'Z';
  }
}