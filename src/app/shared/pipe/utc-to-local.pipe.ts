import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'

@Pipe({name: 'utcToLocal'})
export class UtcToLocalPipe implements PipeTransform {
  transform(value: number) {
    return moment.utc(value).local().format('MMM-DD-YYYY h:mm A');
  }
}