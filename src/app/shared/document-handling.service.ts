import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { ToastService } from 'ng-uikit-pro-standard';
declare const ITHit;

@Injectable({
    providedIn: 'root'
})
export class DocumentHandlingService {

    constructor(
        private service: SharedService,
        public translate: TranslateService,
        public toastr: ToastService,
        private keycloakService: KeycloakService,
    ) {
        this.checkSprigDeviceStatus();
        this.inti()
    }

    userDetails:any = {};
    async inti() {
     
      if (await this.keycloakService.isLoggedIn()) {
        this.userDetails = await this.keycloakService.loadUserProfile();    
      }
    }

    selectedDevice: any = {};
    isSprigEnabled: boolean = false;
    checkSprigDeviceStatus() {
        return new Promise((resolve, reject) => {
            if (localStorage.getItem('selectedDevice'))
                this.selectedDevice = JSON.parse(localStorage.getItem('selectedDevice'));
            if (localStorage.getItem('isSprigEnabled')) {
                this.isSprigEnabled = JSON.parse(localStorage.getItem('isSprigEnabled'));
            }
            resolve('success')
        });
    }


    isEditInPlace(doc) {
        if (this.isSprigEnabled) {
            if (this.ValidateFileExtensions(doc.name))
                return 'edit';
            else
                return 'download';
        }
        else
            this.toastr.warning("Document Service NOT available ")
    }


    editInPlace(doc) {
        return new Promise((resolve, reject) => {
            this.checkSprigDeviceStatus().then(res => {
                const userName = this.userDetails.username;
                if (this.isEditInPlace(doc) == "edit") {
                    //   this.checkReadiness(doc.id, selectedDevice).then(checkFileStatus => {
                    //    if (checkFileStatus) {
                    if (this.selectedDevice.webDavUrl != null) {
                        let sDocumentUrl = this.stripEndingSlash(this.selectedDevice.webDavUrl) + "/User_" + userName + "/" + doc.id + "_123" + "/" + doc.name;           
                        ITHit.WebDAV.Client.DocManager.EditDocument(sDocumentUrl, "/", this.protocolInstallCallback);
                        resolve('done');
                    }
                    else {
                        this.goDownload(doc).then(res => {
                            resolve('done');
                        })
                    }
                    //    }
                    //  })
                }
                else if (this.isEditInPlace(doc) == "download") {
                    this.goDownload(doc).then(res => {
                        resolve('done');
                    })
                }
            })

        })
    }

    // Called if protocol handler is not installed
    protocolInstallCallback(message) {
        var installerFilePath = "assets/WebDAV/Plugins/" + ITHit.WebDAV.Client.DocManager.GetInstallFileName();

        if (confirm("This action requires a protocol installation. Select OK to download the protocol installer.")) {
            window.open(installerFilePath);
        }
    }
    stripEndingSlash(s) {
        if (s != "" && s.endsWith("/")) {
            s = s.substring(0, s.length - 1);
        }
        return s;
    }

    ValidateFileExtensions(sFileName) {
        var _validFileExtensions = [".docx", ".doc", ".docm", ".dot", ".dotm", ".dotx",
            ".xltx", ".xltm", ".xlt", ".xlsx", ".xlsm", ".xlsb", ".xls", ".xll", ".xlam", ".xla",
            ".pptx", ".pptm", ".ppt", ".ppsx", ".ppsm", ".pps", ".ppam", ".ppa", ".potx", ".potm", ".pot",
            ".accdb", ".mdb",
            ".xsn", ".xsf",
            ".pub",
            ".vstx", ".vstm", ".vst", ".vssx", ".vssm", ".vssm", ".vss", ".vsl", ".vsdx", ".vsdm", ".vsd", ".vdw",
            ".mpp",
            ".odt",
            ".ods",
            ".odp"];
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
            if (!blnValid) {
                // alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                return false;
            }
        }
        return true;
    }

    goDownload(doc?: any) {
            return new Promise((resolve, reject) => {
                this.checkSprigDeviceStatus().then(res => {
                const userName = this.userDetails.username;
                this.service
                    .getFile(this.selectedDevice.basePathUrl + '/documents/download/' + doc.id + "?userName=" + userName, {})
                    .subscribe(data => {          
                        if (data != null) {
                            var url = window.URL.createObjectURL(new Blob([data]));
                            var a = window.document.createElement('a');
                            a.href = url;
                            a.download = doc.filename;
                            // a.target = '_blank';
                            document.body.appendChild(a);
                            a.click();
                            setTimeout(function () {
                                window.URL.revokeObjectURL(url);
                            }, 100);
                        }
                        else
                            this.toastr.error("File not found");
                        resolve(data)
                    },err=>{                      
                        this.toastr.error(err.errorMessage)
                    })
            });
        });

    }



}