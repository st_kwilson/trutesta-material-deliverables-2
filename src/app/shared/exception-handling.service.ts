import { Injectable } from '@angular/core';
import { ToastService } from 'ng-uikit-pro-standard';
@Injectable()
export class ExceptionHandlingService {

    constructor(public toastr: ToastService) { }
    errorHandling(data) {
        return new Promise((resolve, reject) => {
            let errorResponse = data.error;
            let toastOptions = {
                positionClass:'md-toast-bottom-right',tapToDismiss: true 
              }
            if(errorResponse){
                this.toastr.error(errorResponse.errorMessage, '' , toastOptions);
                let errorObj = {};
                if (typeof errorResponse.violations != "undefined" && errorResponse.violations !== null) {
                    for (var i = 0; i < errorResponse.violations.length; i++) {
                        let violations = errorResponse.violations[i];
                        errorObj[violations['fieldName']] = { errorMessage: violations['errorMessage'] }
                    }
                    resolve(errorObj)
                }else{
                  resolve(data.error)
                }
            }
        });
    }

}
