const path = require('path');
const express = require('express');
const app = express();

// If an incoming request uses
// a protocol other than HTTPS,
// redirect that request to the
// same url but with HTTPS
const forceSSL = function() {
  return function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    next();
  }
};

// Run the app by serving the static files
// in the dist directory
app.use('/truaccreditation', express.static(__dirname + '/dist/trutesta-material-deliverables'));

// For all GET requests, send back index.html
// so that PathLocationStrategy can be used
app.get('*', function(req, res) {
  if(req.originalUrl == '/assets/servicemanifest.json'){
    res.sendFile(path.join(__dirname + '/dist/trutesta-material-deliverables/assets/servicemanifest.json'));
  }else{
    res.sendFile(path.join(__dirname + '/dist/trutesta-material-deliverables/index.html'));
  }
});

// Start the app by listening on the default
// Heroku port
app.listen(process.env.PORT || 8080);
